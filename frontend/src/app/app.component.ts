import { Component } from '@angular/core';

@Component({
  selector: 'app-carlo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular';
}
