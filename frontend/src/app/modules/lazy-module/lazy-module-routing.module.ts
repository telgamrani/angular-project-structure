import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LazyModuleRootComponent } from '@lazy-module/pages/components/lazy-module-root/lazy-module-root.component';

const routes: Routes = [{ path: '', component: LazyModuleRootComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LazyModuleRoutingModule { }
