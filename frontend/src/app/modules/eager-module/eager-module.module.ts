import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EagerModuleRoutingModule } from '@eager-module/eager-module-routing.module';
import { COMPONENTS_DECLARATIONS } from './pages/components';


@NgModule({
  declarations: [...COMPONENTS_DECLARATIONS],
  imports: [
    CommonModule,
    EagerModuleRoutingModule
  ]
})
export class EagerModuleModule { }
